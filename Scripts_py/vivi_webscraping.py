#Imports
import requests
import pandas as pd
import numpy as np
import re
import os
import logging

from bs4 import BeautifulSoup
from datetime import datetime
from sqlalchemy import create_engine

def data_colletion(url, headers):
   
    #request to URL
    page = requests.get(url, headers = headers)

    #Beutiful soup object
    soup = BeautifulSoup(page.text, 'html.parser')

    #pagnation
    total_item = soup.find('h2', class_='load-more-heading').get('data-total')

    #new URL
    url01 = 'https://www2.hm.com/en_us/men/products/jeans.html?sort=stock&image-size=small&image=model&offset=0&page-size='+ str(total_item)

    #new request URL
    page = requests.get(url01, headers = headers)

    #new Beautiful sopu object
    soup = BeautifulSoup(page.text, 'html.parser')

    #===================== Product Data ==========================
    product = soup.find('ul', class_='products-listing small')
    product_list = product.find_all('article', class_='hm-product-item')

    #product and style id
    product_id = [p.get('data-articlecode') for p in product_list]
    style_id = [p[:-3] for p in product_id]

    #product_name
    product_list = product.find_all('a', class_='link')
    product_name = [p.get_text() for p in product_list]

    #price
    product_list = product.find_all('span', class_='price regular')
    product_price = [p.get_text() for p in product_list]

    # vitrine created
    data = pd.DataFrame([product_id, style_id, product_name, product_price]).T
    data.columns = ['product_id', 'style_id', 'product_name', 'product_price']

    # keeping just the unique product style to join later
    data = data.drop_duplicates(subset=['product_price','style_id'], ignore_index = True)

    return data

def data_by_product(data, headers):

    #empty dataframe
    df_details = pd.DataFrame()

    #unique columns for all products
    aux = []

    #colection of all colors by unique style
    for i in range (len(data)):
        #API requests by len vitrini
        url02 = 'https://www2.hm.com/en_us/productpage.'+ data.loc[i,'product_id']+'.html'
        page_product = requests.get(url02, headers= headers)

        #beautiful soup object by products
        soup = BeautifulSoup(page_product.text, 'html.parser')
        
        # ================================= color_name ========================================= 
        product_list = soup.find_all('a', {'class':['filter-option miniature', 'filter-option miniature active']})
        color_name = [p.get('data-color') for p in product_list]
        
        # =================================== product_id =======================================
        product_id = [p.get('data-articlecode') for p in product_list]
        
        # ================================= DataFrame color ====================================
        df_color = pd.DataFrame([product_id, color_name]).T
        df_color.columns = ['product_id', 'color_name']
        
        # ================================= style_id and color_id ==============================
        df_color['style_id'] = df_color['product_id'].apply(lambda x: x[:-3])
        df_color['color_id'] = df_color['product_id'].apply(lambda x: x[-3:])
        
        cols = ['Art. No.','More sustainable materials', 'product_id', 'Composition', 'Product safety', 'Fit', 'Size']
        df_paterns = pd.DataFrame(columns = cols)

        #collection of details products by colors products
        for j in range(len(df_color)):
            #API request by colors
            url02 = 'https://www2.hm.com/en_us/productpage.' + df_color.loc[j, 'product_id']+'.html'
            page_color = requests.get(url02, headers= headers)
            soup = BeautifulSoup(page_color.text, 'html.parser')

                
            # ============================= description product =====================================
            description = soup.find_all('div', class_= 'pdp-description-list-item')
            product_description = [list(filter(None, p.get_text().split('\n'))) for p in description]

            ## ============ DataFrame composition, rename columns and delet the first row ============
            df_description = pd.DataFrame(product_description).T
            df_description.columns = df_description.iloc[0]
            df_description = df_description.iloc[1:].fillna(method='ffill')
            df_description['product_id'] = df_description['Art. No.']
            
            # ================================= style_id and color_id ===============================
            df_description['color_id'] = df_description['Art. No.'].apply(lambda x: x[-3:])

            aux = aux + df_description.columns.tolist() #Está comentado, pois já foi utilizado para extrair 
            #as colunas padrão e fazer o cols. No fim do for é só: set(aux) e pegar as colunas
            #set(aux) result  ='Art. No.', 'Composition', 'Fit', 'More sustainable materials', 'Product safety', 'Size', 'color_id', 'product_id', 'style_id'
            
            df_paterns = pd.concat([df_paterns, df_description], axis=0)
            
        
        # ================================= merge data color and composition for wich jeans ==============================
        df_sku = pd.merge(df_color, df_paterns[['product_id', 'Fit', 'Size','Composition']], how='left',on='product_id')

        # ================================= all details products ==============================
        df_details = pd.concat([df_details, df_sku], axis=0)
        
        
    # ===== Join showroom data + details
    data_raw = pd.merge(data[['style_id', 'product_name', 'product_price']], df_details, how='left', on='style_id', validate='1:m')

    # data scrapy
    data_raw['scrapy_datetime'] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')

    return data_raw
    
def data_cleanning(data):
    df1 = data.copy()
    # product_id não é necessário alteração
    # product_category não necessta de nenhuma transformação por que os valores já estão no formato correto

    # product_name será trasnformado, retirando os espaço e replace por '_' enquanto as letras minusculos lower 
    df1['product_name'] = df1['product_name'].apply(lambda x: x.replace(' ', '_').lower())

    #produc_price
    df1['product_price'] = df1['product_price'].apply(lambda x: x.replace("$ ", "") if pd.notnull(x) else x).astype(float)

    #color_name
    df1['color_name'] = df1['color_name'].apply(lambda x: x.replace(' ', '_').replace('/', '_').lower() if pd.notnull(x) else x)

    #Fit
    df1['Fit'] = df1['Fit'].apply(lambda x: x.replace(' ', '_').lower() if pd.notnull(x) else x)

    #size vai virar size number e size model (Tem dois tipos de size na linha)
    df1['size_number'] = df1['Size'].apply(lambda x: re.search('\d{3}cm', x).group(0) if pd.notnull(x) else x)
    df1['size_number'] = df1['size_number'].apply(lambda x: re.search('\d+', x).group(0) if pd.notnull(x) else x)

    #size model
    #df1['size_model'] = df1['Size'].str.extract('(\d+/\\d+)')
    df1 = df1.drop(columns=['Size'], axis=1)

    #composition
    df1 = df1[~df1['Composition'].str.contains('Pocket', na=False)]
    df1 = df1[~df1['Composition'].str.contains('Lining', na=False)]
    df1['Composition'] = df1['Composition'].apply(lambda x: x.replace('Shell:', '') if pd.notnull(x) else x)

    # reset index
    df1 = df1.reset_index(drop =True)

    # break composition by comman
    df_c = df1['Composition'].str.split(',', expand=True)
    df_ref = pd.DataFrame(index = np.arange(len(df1)), columns = ['cotton','polyester', 'spandex', 'elasterell'])

    ##cotton
    df_cotton_0 = df_c.loc[df_c[0].str.contains('Cotton', na = True), 0]
    df_cotton_1 = df_c.loc[df_c[1].str.contains('Cotton', na = True), 1]
    #combine
    df_cotton = df_cotton_0.combine_first(df_cotton_1)
    df_cotton.name = 'cotton'
    df_ref = pd.concat([df_ref, df_cotton], axis=1)

    # polyester
    df_polyester_0 = df_c.loc[df_c[0].str.contains('Polyester', na=True), 0]
    df_polyester_1 = df_c.loc[df_c[1].str.contains('Polyester', na=True), 1]
    df_polyester = df_polyester_0.combine_first(df_polyester_1)
    df_polyester.name = 'polyester'
    df_ref = pd.concat([df_ref, df_polyester], axis=1)

    #Spandex
    df_spandex = df_c.loc[df_c[1].str.contains('Spandex', na=False, case=True), 1]
    df_spandex_1 = df_c.loc[df_c[2].str.contains('Spandex', na=False, case=True), 2]
    df_spandex_2 = df_c.loc[df_c[3].str.contains('Spandex', na = False, case=True), 3]
    df_spandex = df_spandex.combine_first(df_spandex_1)
    df_spandex = df_spandex.combine_first(df_spandex_2)
    df_spandex.name = 'spandex'

    df_ref = pd.concat([df_ref, df_spandex], axis=1)

    # Elasterell
    df_elasterell = df_c.loc[df_c[1].str.contains('Elasterell-P', na=True),1]
    df_elasterell.name = 'elasterell'
    df_ref = pd.concat([df_ref, df_elasterell], axis=1)

    # Remove duplicates columns
    df_ref = df_ref.iloc[:, ~df_ref.columns.duplicated(keep='last')]

    # Turn NaN to 0% and formating data
    df_ref['cotton'] = df_ref['cotton'].apply(lambda x: int(re.search('\d+', x).group(0))/100 if pd.notnull(x) else 0)
    df_ref['polyester'] = df_ref['polyester'].apply(lambda x: int(re.search('\d+', x).group(0))/100 if pd.notnull(x) else 0)
    df_ref['spandex'] = df_ref['spandex'].apply(lambda x: int(re.search('\d+', x).group(0))/100 if pd.notnull(x) else 0)
    df_ref['elasterell'] = df_ref['elasterell'].apply(lambda x: int(re.search('\d+', x).group(0))/100 if pd.notnull(x) else 0)

    #Concat df1 and compositions
    df1 = pd.concat([df1, df_ref], axis=1)
    df1 = df1.drop(columns = ['Composition'])

    #drop duplicates
    df1 = df1.drop_duplicates()

    return df1
    
def data_inserting(data):
    # Essa conexão popula uma tabela já criada 
    conn = create_engine('sqlite:///hm_db.sqlite', echo=False)
    data.to_sql('vitrine', con=conn, if_exists='append', index=False )
    return None

if __name__ == '__main__':
    # logging
    path = 'C:\\Users\\vivia\\Workspace\\Repositories\\ds-ao-dev\\Scripts_py\\'
    if not os.path.exists(path+'LogsVivi'):
        os.makedirs(path+'LogsVivi')
    
    logging.basicConfig(
        filename=path+'LogsVivi\\vivi_websraping_hm.log',
        level = logging.DEBUG,
        format = '%(asctime)s - %(levelname)s - %(name)s - %(message)s',
        datefmt ='%Y-%m-%d %H:%M:%S'
    )

    logger = logging.getLogger('vivi_webscraping_hm')

    #parameters
    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}
    #URL
    url = 'https://www2.hm.com/en_us/men/products/jeans.html'

    data_product = data_colletion(url=url, headers=headers)
    logger.info('data colleted')
    data_details = data_by_product(data=data_product, headers=headers)
    logger.info('data details collected')
    data_cleaned = data_cleanning(data=data_details)
    logger.info('data cleanned')
    data_inserting(data=data_cleaned)
    logger.info('data inserted done ')