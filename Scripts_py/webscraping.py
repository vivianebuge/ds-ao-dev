#Imports
import os
import requests
import pandas as pd
import numpy as np
import re
import logging

from sqlalchemy import create_engine
from bs4 import BeautifulSoup
from datetime import datetime

#Data collection
def data_collection(url, headers):

    #request to URL
    page = requests.get(url, headers = headers)

    #Beutiful soup object
    soup = BeautifulSoup(page.text, 'html.parser')

    #pagnation
    total_item = soup.find('h2', class_='load-more-heading').get('data-total')

    #new URL
    url01 = 'https://www2.hm.com/en_us/men/products/jeans.html?sort=stock&image-size=small&image=model&offset=0&page-size='+ str(total_item)

    #new request URL
    page = requests.get(url01, headers = headers)

    #new Beautiful sopu object
    soup = BeautifulSoup(page.text, 'html.parser')

    #===================== Product Data ==========================
    product = soup.find('ul', class_='products-listing small')
    product_list = product.find_all('article', class_='hm-product-item')

    #product and style id
    product_id = [p.get('data-articlecode') for p in product_list]

    #product_name
    product_list = product.find_all('a', class_='link')
    product_name = [p.get_text() for p in product_list]

    #price
    product_list = product.find_all('span', class_='price regular')
    product_price = [p.get_text() for p in product_list]

    # vitrine created
    data = pd.DataFrame([product_id, product_name, product_price]).T
    data.columns = ['product_id', 'product_name', 'product_price']
    
    return data

# Data collection by products details

def data_collection_by_product(data, headers):
    #empty dataframe
    df_compositions = pd.DataFrame()

    #unique columns for all products
    aux = []

    cols = ['Art. No.', 'More sustainable materials', 'Composition', 'Product safety', 'Fit', 'Size']
    df_pattern = pd.DataFrame(columns = cols)

    #colection of all colors by unique style
    for i in range (len(data)):
        #API requests by len vitrini
        url02 = 'https://www2.hm.com/en_us/productpage.'+ data.loc[i,'product_id']+'.html'
        page_product = requests.get(url02, headers= headers)
        logger.debug('Product: %s', url02)
        #beautiful soup object by products
        soup = BeautifulSoup(page_product.text, 'html.parser')
        
        # ================================= color_name ========================================= 
        product_list = soup.find_all('a', {'class':['filter-option miniature', 'filter-option miniature active']})
        color_name = [p.get('data-color') for p in product_list]
        
        # =================================== product_id =======================================
        product_id = [p.get('data-articlecode') for p in product_list]
        
        # ================================= DataFrame color ====================================
        df_color = pd.DataFrame([product_id, color_name]).T
        df_color.columns = ['product_id', 'color_name']
        

        #collection of details products by colors products
        for j in range(len(df_color)):
            #API request by colors
            url03 = 'https://www2.hm.com/en_us/productpage.' + df_color.loc[j, 'product_id']+'.html'
            page_color = requests.get(url03, headers= headers)
            logger.debug('Color: %s', url03)

            #Beautiful object
            soup = BeautifulSoup(page_color.text, 'html.parser')

            # ============================= product name =====================================
            product_name = soup.find_all('h1', class_='primary product-item-headline')
            product_name = product_name[0].get_text()
            
            # ============================= product price =====================================
            product_price = soup.find_all('div', class_= 'primary-row product-item-price')
            product_price = re.findall(r'\d+\.?\d+', product_price[0].get_text())[0]
            
            # ============================= description product =====================================
            description = soup.find_all('div', class_= 'pdp-description-list-item')
            product_description = [list(filter(None, p.get_text().split('\n'))) for p in description]

            ## ============ DataFrame composition and rename columns  ==============================
            df_description = pd.DataFrame(product_description).T
            df_description.columns = df_description.iloc[0]
            
            ## =================== delete firs row ================================================
            df_description = df_description.iloc[1:].fillna(method='ffill')
        
            ## =================== Replace the shelll and Pocket ====================================
            df_description['Composition'] = df_description['Composition'].replace('Shell:', '', regex=True)
            df_description['Composition'] = df_description['Composition'].replace('Pocket Lining:', '', regex=True)
            df_description['Composition'] = df_description['Composition'].replace('Lining:', '', regex=True)
            
            
            #garantee the same number of columns
            df_description = pd.concat([df_pattern, df_description], axis=0)
            
            #rename and add columns
            df_description.columns = ['product_id','sustable_material', 'composition', 'product_safety', 'fit', 'size']
            df_description['product_name'] = product_name
            df_description['product_price'] = product_price
        
            #keep new coluns if it shows up
            aux = aux + df_description.columns.tolist() 
        
            # merge data color + composition
            df_description = pd.merge(df_description, df_color, how='left', on='product_id')
            
            #all products
            df_compositions = pd.concat([df_compositions, df_description], axis=0)
        
        
    # ===== Join showroom data + details
    df_compositions['style_id'] = df_compositions['product_id'].apply(lambda x: x[:-3])
    df_compositions['color_id'] = df_compositions['product_id'].apply(lambda x: x[-3:])

    # data scrapyng
    df_compositions['scrapy_datetime'] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    
    return df_compositions

# Data cleanning
def data_cleaning(df_compositions):
    # product id
    df1 = df_compositions.dropna(subset=['product_id'])

    # product_name será trasnformado, retirando os espaço e replace por '_' enquanto as letras minusculos lower 
    df1['product_name'] = df1['product_name'].str.replace('\n', '')
    df1['product_name'] = df1['product_name'].str.replace('\t', '')
    df1['product_name'] = df1['product_name'].str.replace('  ', '')
    df1['product_name'] = df1['product_name'].str.replace(' ', '_').str.lower()

    #produc_price
    df1['product_price'] = df1['product_price'].astype(float)

    #color_name
    df1['color_name'] = df1['color_name'].str.replace(' ', '_').str.lower()

    #fit
    df1['fit'] = df1['fit'].apply(lambda x: x.replace(' ', '_').lower() if pd.notnull(x) else x)

    #size vai virar size number e size model (Tem dois tipos de size na linha)
    df1['size_number'] = df1['size'].apply(lambda x: re.search('\d{3}cm', x).group(0) if pd.notnull(x) else x)
    df1['size_number'] = df1['size_number'].apply(lambda x: re.search('\d+', x).group(0) if pd.notnull(x) else x)

    #size model
    df1['size_model'] = df1['size'].str.extract('(\d+/\\d+)')

    # break composition by comman
    df2 = df1['composition'].str.split(',', expand=True).reset_index(drop=True)

    # cotton | polyester | elastano | elasterell
    df_ref = pd.DataFrame(index = np.arange(len(df1)), columns = ['cotton','polyester', 'elastane', 'elasterell'])

    ##cotton
    df_cotton_0 = df2.loc[df2[0].str.contains('Cotton', na = True), 0]
    df_cotton_1 = df2.loc[df2[1].str.contains('Cotton', na = True), 1]

    #combine
    df_cotton = df_cotton_0.combine_first(df_cotton_1)
    df_cotton.name = 'cotton'
    df_ref = pd.concat([df_ref, df_cotton], axis=1)

    # polyester
    df_polyester_0 = df2.loc[df2[0].str.contains('Polyester', na=True), 0]
    df_polyester_1 = df2.loc[df2[1].str.contains('Polyester', na=True), 1]
    df_polyester = df_polyester_0.combine_first(df_polyester_1)
    df_ref = pd.concat([df_ref, df_polyester], axis=1)
    df_polyester.name = 'polyester'

    # elastane
    df_elastane_1 = df2.loc[df2[1].str.contains('Elastane', na=False, case=True), 1]
    df_elastane_2 = df2.loc[df2[2].str.contains('Elastane', na=False, case=True), 2]
    df_elastane_3 = df2.loc[df2[3].str.contains('Elastane', na = False, case=True), 3]

    df_elastane_c2= df_elastane_1.combine_first(df_elastane_2)
    df_elastane = df_elastane_c2.combine_first(df_elastane_3)
    df_elastane.name = 'elastane'

    df_ref = pd.concat([df_ref, df_elastane], axis=1)

    # elasterell
    df_elasterell = df2.loc[df2[1].str.contains('Elasterell', na=True),1]
    df_elasterell.name = 'elasterell'

    df_ref = pd.concat([df_ref, df_elasterell], axis=1)

    # Remove duplicates columns
    df_ref = df_ref.iloc[:, ~df_ref.columns.duplicated(keep='last')]

    #join of combine with product_id:
    df_aux = pd.concat([df1['product_id'].reset_index(drop=True), df_ref], axis=1)

    # Turn NaN to 0% and formating data
    df_aux['cotton'] = df_aux['cotton'].apply(lambda x: int(re.search('\d+', x).group(0))/100 if pd.notnull(x) else x)
    df_aux['polyester'] = df_aux['polyester'].apply(lambda x: int(re.search('\d+', x).group(0))/100 if pd.notnull(x) else x)
    df_aux['elastane'] = df_aux['elastane'].apply(lambda x: int(re.search('\d+', x).group(0))/100 if pd.notnull(x) else x)
    df_aux['elasterell'] = df_aux['elasterell'].apply(lambda x: int(re.search('\d+', x).group(0))/100 if pd.notnull(x) else x)

    # final join
    df_aux = df_aux.groupby('product_id').max().reset_index().fillna(0)
    df1 = pd.merge(df1, df_aux, on='product_id', how='left')

    # drop columns
    df1 = df1.drop(columns = ['size', 'product_safety', 'composition'], axis=1)

    #drop duplicates
    df1 = df1.drop_duplicates()

    return df1


# Data Insert
def data_insert(data_cleaned):
    # create database connection
    conn = create_engine('sqlite:///database_hm.sqlite')

    # data insert
    data_insert= data_cleaned[[
        'product_id',
        'style_id',
        'color_id',
        'product_name',
        'color_name',
        'fit',
        'product_price',
        'size_number',
        'size_model',
        'cotton',
        'polyester',
        'elastane',
        'elasterell',
        'scrapy_datetime']]

    data_insert.to_sql('vitrineMei', con=conn, if_exists='append', index=False)
    return None


if __name__ == '__main__':
    # logging
    path = 'C:\\Users\\vivia\\Workspace\\Repositories\\ds-ao-dev\\Scripts_py\\'
    if not os.path.exists(path+'Logs'):
        os.makedirs(path+'Logs')
    
    logging.basicConfig(
        filename=path+'Logs\\websraping_hm.log',
        level = logging.DEBUG,
        format = '%(asctime)s - %(levelname)s - %(name)s - %(message)s',
        datefmt ='%Y-%m-%d %H:%M:%S'
    )

    logger = logging.getLogger('webscraping_hm')

    # parameters and constants
    headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}

    #URL
    url = 'https://www2.hm.com/en_us/men/products/jeans.html'

    # data collection
    data = data_collection( url, headers)
    logger.info("data collect done")

    #data colletion by products
    data_product = data_collection_by_product(data, headers)
    logger.info('data collect by product done')

    #data cleaning
    data_product_cleaned = data_cleaning(data_product)
    logger.info('data product cleaned done')

    # data insertion
    data_insert(data_product_cleaned)
    logger.info('data inserted done ')